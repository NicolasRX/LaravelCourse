@extends('layouts.app')

@section('content')
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Museums Finder
                    <a id="js-btn-show-notification" href="#" class="btn">Open Notication</a>
                </div>

                <div class="links">
                    <a href="{{ url('museums') }}">Museums</a>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script>
        (function ($) {
            var $btn = $('#js-btn-show-notification');
            $btn.on('click', function () {
                new PNotify({
                    title: '¡Welcome Bro!',
                    text: 'Whats Up! Welcome to the Museums Finder',
                    type:'info'
                });
            });



        })(jQuery);
    </script>
@endsection