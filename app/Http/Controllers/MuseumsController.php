<?php

namespace App\Http\Controllers;

use App\Museum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MuseumsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $museums = Museum::paginate(1);
        return view('museums.index')->with('museums', $museums);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('museums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required'
        ];
        $messages = [
            'name.required' => 'The field "name" is required',
            'description.required' => 'The field "Description" is required'
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            flash("We couldn't save your museum :c")->error();
            return redirect()->action('MuseumsController@create')
                ->withErrors($validator)
                ->withInput();
        }

        $museum = new Museum();
        $museum->name                        = $request->input('name');
        $museum->description                 = $request->input('description');
        $museum->phone= "";
        $museum->rating = "4.8";
        $museum->address = "Cra 27c #105-81";
        $museum->hours = "L-V, 6Am - 7PM";

        $museum->save();

        flash('Your Museum has been saved bro!')->success();
        return redirect()->action('MuseumsController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $museum = Museum::findOrFail($id);

        return view('museums.show', compact('museum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
